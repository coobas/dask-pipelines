__all__ = ["pipeline_component", "create_actor", "create_pipeline"]

import functools
import random
import string
from typing import Any, Callable, Dict, Optional, TypeVar, cast

import distributed.actor
import distributed.client


def _get_future_result(arg: Any) -> Any:
    """Gets the result value if arg comes from an Actor"""
    if isinstance(arg, distributed.actor.ActorFuture):
        return arg.result()
    return arg


_F = TypeVar("_F", bound=Callable[..., Any])


def pipeline_component(func: _F) -> _F:
    """Decorator for pipeline functions"""

    @functools.wraps(func)
    def wrapper(*args: Any, **kwargs: Any) -> Any:
        # Ignoring mypy, it assumes the args is Tuple[Any, ...] which is changed to Iterator in the line below.
        # Which for the functionality of this function is ok.
        args = map(_get_future_result, args)  # type: ignore
        kwargs = {key: _get_future_result(value) for key, value in kwargs.items()}
        return func(*args, **kwargs)

    return cast(_F, wrapper)


def create_actor(
    dask_client: distributed.client.Client, cls: type
) -> distributed.actor.Actor:
    actor_future = dask_client.submit(cls, actor=True)
    return actor_future.result()


# TODO this should be a class
def create_pipeline(
    dask_client: distributed.client.Client, pipeline_definition: _F, output_node: str
) -> _F:
    """Create a pipeline function from a pipeline definition and the name of the output node"""

    def _get_result(*args, **kwargs):
        dsk, output = _make_keys_unique(
            pipeline_definition(*args, **kwargs), output_node=output_node
        )
        return dask_client.get(dsk, output)

    return cast(_F, _get_result)


def _make_keys_unique(
    dsk: Any, keys: Optional[Dict[str, Any]] = None, output_node: Optional[str] = None
) -> Any:
    """
    Transform a Dask graph to an equivalent one with globally unique keys

    This is because Dask does not evaluate properly multiple graphs with overlapping
    node hashes if submitted with minimum latency. This is a Dask problem,
    which is unlikely to be fixed easily.

    :param dsk: Dask graph (dict) or a single node
    :param keys: already assigned mapping to unique keys (for recursive use)
    :output_node: if given, returns also the unique output node name
    """
    if keys is None:
        suffix = "_" + "".join(random.choices(string.ascii_letters, k=3))
        keys = {key: key + suffix for key in dsk.keys()}

    result: Any
    if isinstance(dsk, dict):
        result = {keys[key]: _make_keys_unique(node, keys) for key, node in dsk.items()}
    elif isinstance(dsk, tuple):
        result = tuple(_make_keys_unique(node, keys) for node in dsk)
    elif isinstance(dsk, list):
        result = list(_make_keys_unique(node, keys) for node in dsk)
    else:
        result = keys.get(dsk, dsk)

    if output_node is None:
        return result
    return result, output_node + suffix
